﻿using System;
using System.Collections;
using BepInEx;
using BepInEx.Configuration;
using UnityEngine;
using Random = UnityEngine.Random;

namespace ULTRAPAIN {
    [BepInPlugin(pluginGuid, pluginName, pluginVersion)]
    public class PainOfUltra : BaseUnityPlugin {
        public const string pluginGuid = "robiehh.uk.pain";
        public const string pluginName = "ULTRAPAIN";
        public const string pluginVersion = "1.4";

        public ConfigEntry<int> interval;
        
        public PainOfUltra() {
            interval = Config.Bind("ULTRAPAIN", "Interval", 10, new ConfigDescription("The amount of time that has to pass for reroll"));
        }
        
        void Start() {
            StartCoroutine(ChangeLoadout());
        }

        VariantSetting RandomVariant {
            get {
                VariantSetting variantSetting = ForceOff;
                int random = Random.Range(0, 2);
                switch (random) {
                    case 0:
                        variantSetting.blueVariant = VariantOption.ForceOn;
                        break;
                    case 1:
                        variantSetting.greenVariant = VariantOption.ForceOn;
                        break;
                }

                return variantSetting;
            }
        }

        VariantSetting ForceOff =>
            new VariantSetting {
                blueVariant = VariantOption.ForceOff,
                greenVariant = VariantOption.ForceOff,
                redVariant = VariantOption.ForceOff
            };

        VariantSetting RandomVariantRed {
            get {
                VariantSetting variantSetting = ForceOff;
                int random = Random.Range(0, 3);
                switch (random) {
                    case 0:
                        variantSetting.blueVariant = VariantOption.ForceOn;
                        break;
                    case 1:
                        variantSetting.greenVariant = VariantOption.ForceOn;
                        break;
                    case 2:
                        variantSetting.redVariant = VariantOption.ForceOn;
                        break;
                }

                return variantSetting;
            }
        } 
        
        ArmVariantSetting RandomArmVariant {
            get {
                ArmVariantSetting variantSetting = new ArmVariantSetting {
                    blueVariant = VariantOption.ForceOff,
                    greenVariant = VariantOption.ForceOff,
                    redVariant = VariantOption.ForceOff
                };
                int random = Random.Range(0, (MonoSingleton<FistControl>.Instance.heldObject && MonoSingleton<FistControl>.Instance.heldObject.gameObject != null) ? 2 : 3);
                switch (random) {
                    case 0:
                        variantSetting.blueVariant = VariantOption.ForceOn;
                        break;
                    case 1:
                        variantSetting.greenVariant = VariantOption.ForceOn;
                        break;
                    case 2:
                        variantSetting.redVariant = VariantOption.ForceOn;
                        break;
                }

                return variantSetting;
            }
        } 

        IEnumerator ChangeLoadout() {
            yield return new WaitForSeconds(interval.Value);
            try {
                ForcedLoadout loadout = new ForcedLoadout();
                loadout.arm = RandomArmVariant;
                if (Random.Range(0, 2) == 1) {
                    loadout.revolver = RandomVariant;
                    loadout.altRevolver = ForceOff;
                } else {
                    loadout.altRevolver = RandomVariant;
                    loadout.revolver = ForceOff;
                }

                if (Random.Range(0, 2) == 1) {
                    loadout.nailgun = RandomVariant;
                    loadout.altNailgun = ForceOff;
                } else {
                    loadout.altNailgun = RandomVariant;
                    loadout.nailgun = ForceOff;
                }

                loadout.railcannon = RandomVariantRed;
                loadout.shotgun = RandomVariant;

                MonoSingleton<GunSetter>.Instance.forcedLoadout = loadout;
                MonoSingleton<GunSetter>.Instance.ResetWeapons(false);
                MonoSingleton<FistControl>.Instance.forcedLoadout = loadout;
                MonoSingleton<FistControl>.Instance.ResetFists();

                MonoSingleton<GunControl>.Instance.SwitchWeapon(MonoSingleton<GunControl>.Instance.lastUsedSlot, MonoSingleton<GunControl>.Instance.slots[MonoSingleton<GunControl>.Instance.lastUsedSlot - 1], true, false);
                MonoSingleton<GunControl>.Instance.SwitchWeapon(MonoSingleton<GunControl>.Instance.lastUsedSlot, MonoSingleton<GunControl>.Instance.slots[MonoSingleton<GunControl>.Instance.lastUsedSlot - 1], true, false);
                
            } catch (Exception ex) {
                // idc
            }

            Debug.Log("Loadout changed");

            StartCoroutine(ChangeLoadout());
        }
    }
}